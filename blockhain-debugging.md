## Blockchain Debuging (not syncing)

1. You will need to connect to your VPS and run this command:
```
cd /root/.radiuscore
radiusd -rescan -reindex-chainstate -upgradewallet
```
2. Wait to be finished and restart your wallet.
```
systemctl stop Radius.service
sleep 3
systemctl start Radius.service
systemctl enable Radius.service
systemctl status Radius.service
```
3. Let it resync back with the network. The blockchain should be download in less than a day.
4. **VOILÀ** !
