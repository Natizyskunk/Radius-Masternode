A) CREATING AN LVM2 LOGICAL VOLUME FOR SWAP:
```
mkdir /dev/VolGroup00/LogVol02
lvm lvcreate VolGroup00 -n LogVol02 -L 256M
# mkswap /dev/VolGroup00/LogVol02
vim /etc/fstab
```
Add the following entry to the /etc/fstab file: <BR />
/dev/VolGroup00/LogVol02   swap     swap    defaults     0 0

Enable the extended logical volume:
```
swapon -va
```
Test that the logical volume has been extended properly:
```
cat /proc/swaps # free
```

B) CREATING A SWAPFILE <BR />
To calculate swapfile size => 1024 * RAM amount in MB

To create swapfile:
```
sudo -i
swapoff -a
dd if=/dev/zero of=/mnt/swapfile bs=1024 count=2097152
mkswap /mnt/swapfile
vim /etc/fstab
```
And add following line: <BR />
/mnt/swapfile	swap	swap	defaults	0 0 <BR />
/mnt/swapfile none 	swap 	sw 			  0 0

Now lets activate the swap space into system:
```
swapon -a
```

Lets check the active swap space now:
```
free -m
```

or

```
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
sudo mkswap /var/swap.img
sudo swapon /var/swap.img
```

To make the swap file persist if the server is rebooted:
```
sudo chmod 0600 /var/swap.img
sudo chown root:root /var/swap.img
sudo nano /etc/fstab
```
Append the following line to the end of the file: <BR />
var/swap.img none swap sw 0 0 <BR />
 Save the file by using ctrl+x and Yes and Confirm with Enter.
 
  
-----------------------------------
// .Desirecore SwapFile //
```
cd /
sudo dd if=/dev/zero of=swapfile bs=1M count=3000
sudo mkswap swapfile
sudo swapon swapfile
sudo nano etc/fstab
```
/swapfile none swap sw 0 0


-----------------------------------
// Masternodes Guides SwapFile // <BR />
For instance, in our example, we're looking to create a 3 Gigabyte file:
```
fallocate -l 3G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo -e "/swapfile none swap sw 0 0 \n" >> /etc/fstab
```


-----------------------------------
// digitalocean SwapFile // <BR />
For instance, in our example, we're looking to create a 4 Gigabyte file. We can do this by specifying a block size of 1 Gigabyte and a count of 4:
```
sudo dd if=/dev/zero of=/swapfile bs=1G count=4
ls -lh /swapfile
sudo chmod 600 /swapfile
```
Verify that the file has the correct permissions by typing:
```
ls -lh /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon -s
free -m
sudo nano /etc/fstab
```
/swapfile   none    swap    sw    0   0

We can see the current swappiness value by typing:
```
cat /proc/sys/vm/swappiness
```
10

or

we ca use the sysctl command.
```
sudo sysctl vm.swappiness=10
```

set this value automatically at restart:
```
sudo nano /etc/sysctl.conf
```
vm.swappiness=10

Another related value that you might want to modify is the vfs_cache_pressure:
```
cat /proc/sys/vm/vfs_cache_pressure
```
50

or

```
sudo sysctl vm.vfs_cache_pressure=50
sudo nano /etc/sysctl.conf
```
vm.vfs_cache_pressure = 50


-----------------------------------
// The Faster Way SwapFile // <BR />
The quicker way of getting the same file is by using the fallocate program. This command creates a file of a preallocated size instantly, without actually having to write dummy contents. <BR />
We can create a 4 Gigabyte file by typing:
```
sudo fallocate -l 4G /swapfile
```
The prompt will be returned to you almost immediately. We can verify that the correct amount of space was reserved by typing:
```
ls -lh /swapfile
sudo chmod 600 /swapfile
```
Verify that the file has the correct permissions by typing:
```
ls -lh /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon -s
free -m
sudo nano /etc/fstab
```
/swapfile   none    swap    sw    0   0

We can see the current swappiness value by typing:
```
cat /proc/sys/vm/swappiness
```
10

or

we ca use the sysctl command.
```
sudo sysctl vm.swappiness=10
```

set this value automatically at restart:
```
sudo nano /etc/sysctl.conf
```
vm.swappiness=10

Another related value that you might want to modify is the vfs_cache_pressure:
```
cat /proc/sys/vm/vfs_cache_pressure
```
50

or

```
sudo sysctl vm.vfs_cache_pressure=50
sudo nano /etc/sysctl.conf
```
vm.vfs_cache_pressure = 50


-------------------------------
// Snowgem SwapFile //
```
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
```
Making Swap file permanent (optional): We have our swap file enabled, but when we reboot, the server will not automatically enable the file. We can change that by modifying the fstab file.
```
sudo nano /etc/fstab
```
At the bottom of the file, you need to add a line that will tell the operating system to automatically use the file you created: <BR />
/swapfile none swap sw 0 0


-------------------------------
// small VPS Swapfile //
```
sudo dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
sudo mkswap /var/swap.img
sudo swapon /var/swap.img
```

To make the swap file persist when the server is rebooted:
```
sudo chmod 0600 /var/swap.img
sudo chown root:root /var/swap.img
sudo nano /etc/fstab
```

Append the following line to the end of the file: <BR />
/var/swap.img none swap sw 0 0
